from flask import Flask, render_template, request
import requests as make_request
import copy
import pickle
import os
app = Flask(__name__)
api_url = "https://experimentation.getsnaptravel.com/interview/hotels"

@app.route("/")
def hello():
  return "Hello World!"

@app.route('/form/')
def render_static():
  return render_template('form.html')

@app.route("/form/submit/", methods=["POST", "GET"])
def submit_form():
  # list of tuples of (API post params, results) 
  cache_exists = os.path.isfile('cache_file')
  if cache_exists:
    cache_file = open('cache_file','rb')
    try:
      api_cache = pickle.load(cache_file)
    except:
      api_cache = []
    cache_file.close()
  else:
    print('No cache file!')
    api_cache = []

  if request.method == 'POST':
    payload_copy = {
      'city': request.form['city'],
      'checkin': request.form['checkin'],
      'checkout': request.form['checkout'],
    }

    results = []
    for item in api_cache:
      if item[0]['city'] == payload_copy['city'] and item[0]['checkin'] == payload_copy['checkin'] and item[0]['checkout'] == payload_copy['checkout']:
        results = item[1]
        print('CACHE HIT')

    if len(results) == 0:
      cache_file = open('cache_file','wb')
      snaptravel_payload = copy.deepcopy(payload_copy)
      snaptravel_payload['provider'] = 'snaptravel'
      snaptravel_results = make_request.post(api_url, snaptravel_payload)

      retail_payload = copy.deepcopy(payload_copy)
      retail_payload['provider'] = 'retail'
      retail_results = make_request.post(api_url, retail_payload)

      # id to dictionary mapping
      hotel_dictionary = {}
      for hotel in snaptravel_results.json()['hotels']:
        hotel_dictionary[hotel['id']] = hotel['price']

      results = []
      for hotel in retail_results.json()['hotels']:
        if hotel['id'] in hotel_dictionary:
          hotel['snaptravel_price'] = hotel_dictionary[hotel['id']]
          results.append(hotel)

      print([result['id'] for result in snaptravel_results.json()['hotels']])
      print([result['id'] for result in retail_results.json()['hotels']])
      print([result['id'] for result in results])
      api_cache.append((payload_copy, results))
      pickle.dump(api_cache, cache_file)
      cache_file.close()

    return render_template("results.html",results = results)

